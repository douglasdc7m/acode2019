# Acode2019
README
This project has artifacts related to Avenue Code chalenge to QA Engenering occupation.

All the asked steps were performed successfully.

- A manual tests based on the user stories was done and bugs and possible enhancements were reported on the bug tracking tool. 
- It as build an Automated UI Tests using BDD (Behavior Driven-Development) approach based on the bugs found on the above step and the user stories.
- Cucumber, Selenium-webdriver, JUnit and Eclipse IDE were the chosen technologies to implement this. Any record and play were used. The script was coded from scratch.
- The PageObject model (POM) was used as an approach for the test. In this case a class called PageObject.java is used as the repository for HTML elements.
	  This class was built in a way to permit the Auto Suggestion in Eclipse for nested elements. This approach make the codification easier.
	  Eg.: in a Cucumber step definition class, typing PageObject and pressing dot (.) a bunch of application areas are suggested like "HomePage", "TaskPage" and "SubtaskPage".
	  So, choosing "PageObject.HomePage" as an example and performing the same Auto Suggestion described above new elements can be put in intuiteve code chain.
	  This is like the PageObject.java class can be used to implent a click action on My Task menu: PageObject.HomePage.myTasksMenu(driver).click();
	  The "driver" element is the browser itself.
- It includes also a PDF file describing what .other kinds of automated tests can be performed in this application (functional and non-functional), how would maximize the value of testing in this application(testing techniques), all other important considerations.

